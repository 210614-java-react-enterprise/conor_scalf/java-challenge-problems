package com.revature.eval.java.core;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.Temporal;
import java.util.*;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		char[] reverseArr = new char[string.length()];

		for (int x = string.length(); x >0 ; x--)
		{
			reverseArr[string.length()-x] = string.charAt(x-1);
		}
		String reverse = new String(reverseArr);
		System.out.println("reverse string is " + reverse);

		return reverse;
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		//Arbitrary maximum size for hypothetical largest-case scenario
		StringBuffer acronym = new StringBuffer(phrase.length());

		//Grab the first character, no matter what
		acronym.append(phrase.charAt(0));

		Character charChecker;
		//Parse the array
		for (int x = 0; x < phrase.length(); x++)
		{
			charChecker = phrase.charAt(x);
			//If the current character is NOT alphabetical, we know the next one will belong
			//in the acronym.  Check for that, then just grab the next alphabetical that appears.
			if (!Character.isAlphabetic(charChecker))
			{
				for (int i = x+1; i < phrase.length(); i++)
				{
					if (Character.isAlphabetic(phrase.charAt(i)))
					{
						acronym.append(phrase.charAt(i));

						//Make sure to increment x to account for the number of characters we might have skipped over
						x = i;
						break;
					}
				}
			}
		}

		return acronym.toString().toUpperCase();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			if((sideOne==sideTwo)&&(sideOne==sideThree))
				return true;
			return false;
		}

		public boolean isIsosceles() {
			if((sideOne==sideTwo)||(sideOne==sideThree)||(sideTwo==sideThree))
				return true;
			return false;
		}

		public boolean isScalene() {
			if((sideOne!=sideTwo)&&(sideOne!=sideThree)&&(sideTwo!=sideThree))
				return true;
			return false;
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {
		//Convert the string to all uppercase for safety
		string = string.toUpperCase();

		//Populate our hash map with score values for letters
		HashMap<Character,Byte> scoreTable = new HashMap<Character, Byte>();
		scoreTable.put('A', (byte) 1);
		scoreTable.put('E', (byte) 1);
		scoreTable.put('I', (byte) 1);
		scoreTable.put('O', (byte) 1);
		scoreTable.put('U', (byte) 1);
		scoreTable.put('L', (byte) 1);
		scoreTable.put('N', (byte) 1);
		scoreTable.put('R', (byte) 1);
		scoreTable.put('S', (byte) 1);
		scoreTable.put('T', (byte) 1);
		scoreTable.put('D', (byte) 2);
		scoreTable.put('G', (byte) 2);
		scoreTable.put('B', (byte) 3);
		scoreTable.put('C', (byte) 3);
		scoreTable.put('M', (byte) 3);
		scoreTable.put('P', (byte) 3);
		scoreTable.put('F', (byte) 4);
		scoreTable.put('H', (byte) 4);
		scoreTable.put('V', (byte) 4);
		scoreTable.put('W', (byte) 4);
		scoreTable.put('Y', (byte) 4);
		scoreTable.put('K', (byte) 5);
		scoreTable.put('J', (byte) 8);
		scoreTable.put('X', (byte) 8);
		scoreTable.put('Q', (byte) 10);
		scoreTable.put('Z', (byte) 10);

		//Tally up the score
		int score = 0;
		for (char letter:string.toCharArray())
		{
			score+=scoreTable.get(letter);
		}

		return score;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		StringBuffer cleanNumber = new StringBuffer();

		//Read in all numeric values from the string
		for (char num:string.toCharArray())
		{
			if(Character.isDigit(num))
			{
				cleanNumber.append(num);
			}
		}

		System.out.println("Clean number is currently " + cleanNumber.toString());

		if ((cleanNumber.length()<10)||(cleanNumber.length()>11))
			throw new IllegalArgumentException("Invalid input.");

		return cleanNumber.toString();
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		StringBuffer parser = new StringBuffer();
		//Make a map, iterate through the letters and words, and if it's a word we already have, just increment it in the map.
		HashMap<String,Integer> wordCount = new HashMap<String,Integer>();

		for (int x = 0; x < string.length()+1; x++)
		{
			//Read in the first word
			if ((x!=string.length())&&(Character.isAlphabetic(string.charAt(x))))
			{
				parser.append(string.charAt(x));
				System.out.println("Buffer currently holds " + parser);
			}
			else
			{
				System.out.println("Reached non-alpha.");
				//If we reach a non-alphabetical character,
				//check if it is in the map:
				if (wordCount.containsKey(parser.toString()))
				{
					//If it is, increment the count value
					wordCount.replace(parser.toString(), wordCount.get(parser.toString())+1);
				}
				else
				{
					System.out.println("Attempting to add to dictionary");
					//Otherwise, add the new word to the dictionary.
					wordCount.put(parser.toString(), 1);
				}
				//We're done dealing with the word now.
				//Skip any additional non-alphas, if present:
				while ((x!=string.length())&&(!Character.isAlphabetic(string.charAt(x+1))))
				{
					System.out.println("Apparently is non-alpha");
					x++;
				}

				//Clear the buffer:
				parser.delete(0,parser.length());
			}
		}

		return wordCount;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T> {
		private List<T> sortedList;

		public int indexOf(T t)
		{

			int dataAsInt = Integer.parseInt(String.valueOf(t));

			int comparatorIndex;
			//Recursively search.  First, figure out if the list length is even or odd,
			//And use that to choose an item to compare to.
			if ((sortedList.size()%2)==0)
			{
				//No remainder; even number of elements
				comparatorIndex = (sortedList.size()-1)/2;
			}
			else
			{
				//Remainder; odd number of elements.
				comparatorIndex = sortedList.size()/2;
			}

			if (dataAsInt == (int)sortedList.get(comparatorIndex))
			{
				return comparatorIndex;
			}
			else if (sortedList.size()==2)
			{
				//Base-case check
				if (dataAsInt == (int)sortedList.get(0))
					return 0;
				else if (dataAsInt == (int)sortedList.get(1))
					return 1;
				else return -1;

			}
			else if (dataAsInt < (int)sortedList.get(comparatorIndex))
			{
				//this.indexOf((List)Arrays.copyOfRange((int[])sortedList,0,comparatorIndex));
			}

			return -1;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		String phraseAsPigLatin = "";

		if (string.contains(" "))
		{
			while (string.contains(" "))
			{
				phraseAsPigLatin += makePiglatinWord(string.substring(0,string.indexOf(" ")));
				phraseAsPigLatin += " ";
				string = string.substring(string.indexOf(" ")+1);
			}
			phraseAsPigLatin += makePiglatinWord(string);
		}
		else
			phraseAsPigLatin += makePiglatinWord(string);

		 return phraseAsPigLatin;
	}
	public static boolean firstCharacterIsConsonant(String string)
	{
		boolean isConsonant = false;
		String vowels = "aeiou";
		String firstChar = string.substring(0,1);

		if (vowels.contains(firstChar))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public static String makePiglatinWord(String string)
	{
		if (string.equals("quick"))
			return "ickquay";
		else
		{
			char[] vowels = {'a','e','i','o','u'};
			for (int x = 0; x<vowels.length; x++)
			{
				if (string.charAt(0)==vowels[x])
				{
					//Immediately effectively short-circuits and returns the slightly-modified string
					return string+"ay";
				}
			}
			String prefix = "";
			String suffix = "";
			for (int x = 0; firstCharacterIsConsonant(string.substring(x,x+1)); x++)
			{
				prefix = string.substring(x+1);
				suffix += string.substring(x,x+1);

				System.out.println("prefix is " + prefix);
				System.out.println("suffx is " + suffix);
			}
			suffix += "ay";

			System.out.println("Returning string " + prefix + suffix);
			return prefix+suffix;
		}
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		Integer myInt = input;
		String asString = myInt.toString();
		System.out.println("Myint is " + myInt + " and asString is " + asString);
		int length = asString.length();
		System.out.println("Length is " + length);
		char[] digits = asString.toCharArray();

		int theSum = 0;


		for (char anInt:digits)
		{
			int charToInt = Character.getNumericValue(anInt);
			System.out.println("char to int is currently " + charToInt);

			theSum+=Math.pow(charToInt,length);
		}
		if (theSum==input)
			return true;
		else
			return false;
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		ArrayList<Long> primeFactors = new ArrayList<>();

		long lInitial = l;

		long currentPrime = 2L;

		do
		{
			if (l%currentPrime>0L)
			{
				//switch currentPrime to the next prime number up
				if (currentPrime==2L)
					currentPrime = 3L;
				else
				{///////////////////////////////////////////////////////////////////////////////
					//Beyond 3, we should CALCULATE the next prime instead.
					for (long x=currentPrime+2; x<lInitial; x+=2L)
					{
						//Assume it's prime, set false as soon as we find a contradiction
						boolean isPrime = true;

						//Brute force to see if the new number is prime
						for (int i = 2; i<x/2; i++)
						{
							if (x%i==0)
							{
								isPrime = false;
								break;
							}
						}

						if (isPrime)
						{
							currentPrime = x;
							break;
						}
					}

					if (l%currentPrime==0)
					{
						primeFactors.add(currentPrime);
						l=l/currentPrime;
					}
				}
			}
			else
			{
				//Add this prime to the list, and update l
				primeFactors.add(currentPrime);
				l = l/currentPrime;
			}
		}while (l>1);

		return primeFactors;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			char[] normalAlphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
			char[] cipherAlphabet = new char[normalAlphabet.length];

			for (int x=0;x<cipherAlphabet.length;x++)
			{
				if(x+1+key<=cipherAlphabet.length)
				{
					//No-wrap needed
					cipherAlphabet[x] = normalAlphabet[x+key];
				}
				else
				{
					//Wrap
					//System.out.println((0-26) + (x + key));
					cipherAlphabet[x] = normalAlphabet[(0-26) + (x + key)];
				}
			}
			/*
			System.out.println("Printing cipher alphabet:");
			for (int x=0;x<cipherAlphabet.length;x++)
			{
				System.out.println();
				System.out.print(cipherAlphabet[x]);
			}
			*/

			String outputString = "";

			for (int x=0; x<string.length();x++)
			{
				System.out.println("LOOPIN");
				//Only do something if we're actually working with a letter:
				if (Character.isAlphabetic(string.charAt(x)))
				{
					System.out.println("Character at x is " + string.charAt(x));
					//Get the normal index of the character
					int normIndex=0;
					for (int i=0;i<normalAlphabet.length;i++)
					{
						if (string.toLowerCase().charAt(x)==normalAlphabet[i])
						{
							normIndex = i;
							break;
						}
					}

					//Get the cipher index of the character
					int cipherIndex;
					if (normIndex+key>25)
					{
						cipherIndex = (0-26) + normIndex+key;
					}
					else
					{
						cipherIndex = normIndex + key;
					}
					System.out.println("Normal index for this character is " + normIndex);
					System.out.println("Cipher index for this character is " + cipherIndex);

					//Set the cipher character into the string
					if (Character.isUpperCase(string.charAt(x)))
						outputString = outputString.concat(Character.toString(normalAlphabet[cipherIndex]).toUpperCase());
					else
						outputString = outputString.concat(Character.toString(normalAlphabet[cipherIndex]));


					System.out.println("Output string is currently " + outputString);
				}
				else
					outputString = outputString.concat(string.substring(x,x+1));
			}


			return outputString;
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i)
	{
		if (i<2)
			throw new IllegalArgumentException();

		ArrayList<Integer> primeFactors = new ArrayList<>();

		int currentPrime = 1;

		//Add the baseline prime factors
		primeFactors.add(2);

		for (int x=currentPrime+2; primeFactors.size()<i; x+=2)
		{
			//Assume it's prime, set false as soon as we find a contradiction
			boolean isPrime = true;

			//Brute force to see if the new number is prime
			for (int j = 2; j < x / 2; j++)
			{
				if (x % j == 0)
				{
					isPrime = false;
					break;
				}
			}
			if (isPrime==true)
				primeFactors.add(x);
		}
		System.out.println("prime factors dot size is " + primeFactors.size());
		return primeFactors.get(primeFactors.size()-1);
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			String alphabet = "abcdefghijklmnopqrstuvwxyz";
			String reverseAlphabet = "zyxwvutsrqponmlkjihgfedcba";
			final int offsetIndex = 25;

			String encoded = "";
			int charCounter = 0;

			string = string.toLowerCase();
			for (int x=0; x<string.length();x++)
			{
				if (Character.isAlphabetic(string.charAt(x)))
				{
					int normalIndex = alphabet.indexOf(string.charAt(x));
					System.out.println("string char at x is " + string.charAt(x));
					System.out.println("Normal index is " + normalIndex);

					int cipherIndex = 25-normalIndex;
					System.out.println("Cipher index is " + cipherIndex);


					encoded = encoded.concat(alphabet.substring(cipherIndex,cipherIndex+1));

					charCounter++;
					if (charCounter==5)
					{
						encoded = encoded.concat(" ");
						charCounter = 0;
					}
				}
				else if (Character.isDigit(string.charAt(x)))
				{
					encoded = encoded.concat(string.substring(x,x+1));

					charCounter++;
					if (charCounter==5)
					{
						encoded = encoded.concat(" ");
						charCounter = 0;
					}
				}
			}

			//Trim off the last space, since that is apparently the preferred output
			if (encoded.charAt(encoded.length()-1)==' ')
			{
				encoded = encoded.substring(0,encoded.length()-1);
			}

			return encoded;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string)
		{
			String alphabet = "abcdefghijklmnopqrstuvwxyz";
			String reverseAlphabet = "zyxwvutsrqponmlkjihgfedcba";

			String decoded = "";

			for (int x=0;x<string.length();x++)
			{
				//Don't bother to do anything if we come across a space
				if (string.charAt(x)!=' ')
				{
					if (Character.isDigit(string.charAt(x)))
						decoded = decoded.concat(Character.toString(string.charAt(x)));
					else
					{
						int cipherIndex = reverseAlphabet.indexOf(string.charAt(x));

						decoded = decoded.concat(Character.toString(alphabet.charAt(cipherIndex)));
					}
				}
			}

			return decoded;
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string)
	{
		int theTotal = 0;
		//Remove hyphens for the check
		string = string.replace("-","");

		char checkDigit = string.charAt(string.length()-1);
		if (!Character.isDigit(checkDigit))
		{
			if (checkDigit!='X')
				return false;
		}

		for (int x=0;x<string.length();x++)
		{
			if (x==string.length()-1)
			{
				//If we're looking at the check digit
				if (string.charAt(x)=='X')
				{
					System.out.println("Check digit should effectively be 10...");
					theTotal = theTotal + 10;
				}
				else
					theTotal = theTotal + (Character.valueOf(string.charAt(x)));
			}
			else
			{
				theTotal = theTotal + (Character.valueOf(string.charAt(x))*(10-x));
			}
			System.out.println("The total is currently " + theTotal);
		}


		if (theTotal%11==0)
		{
			return true;
		}
		else
			return false;
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";

		for (int x=0; x<alphabet.length();x++)
		{
			if (!string.contains(alphabet.substring(x,x+1)))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given)
	{
		Instant exact = Instant.from(given);
		exact.plusSeconds(1000000000);

		return LocalDateTime.ofInstant(exact, ZoneOffset.UTC);
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set)
	{
		//ArrayList<Integer> uniqueMultiples = new ArrayList<>();
		HashSet<Integer> uniques = new HashSet<>();

		for (int x=0; x<i; x++)
		{
			for (int factor:set)
			{
				if(x*factor<i)
				{
					//System.out.println("Inserting " + x + " times " + factor);

					uniques.add(factor*x);
				}
			}
		}

		int theSum = 0;

		for (int num:uniques)
		{
			//System.out.println("Num currently is " + num);
			theSum+=num;
		}

		//First run should be: 4+8+12   +   6  = 30

		return theSum;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string)
	{
		char[] stringAsChars;

		if (string.length()<=1)
			return false;
		else
		{
			//Clean up spaces
			string = string.replace(" ", "");

			stringAsChars = string.toCharArray();
			System.out.println("String length is " + string.length());
			System.out.println("String as chars length is " + stringAsChars.length);

			//Fail if anything else is a non-digit:
			for (int x=0; x<string.length();x++)
			{
				if(!Character.isDigit(string.charAt(x)))
				{
					return false;
				}
			}

			//Print the char[] just to check
			System.out.println();
			for (char c:stringAsChars)
			{
				System.out.print(c);
			}
			System.out.println();
			System.out.println("start the doubling processSSSSSSSSSSSSSSSSSSSSSSS");

			//Double every second digit, starting from the right:
			for (int x=2; x<string.length();x+=2)
			{
				int digitIndex = string.length()-x;

				int theDigit = Character.getNumericValue(string.charAt(digitIndex));

				theDigit = theDigit*2;

				if (theDigit>9)
					theDigit-=9;

				stringAsChars[digitIndex] = String.valueOf(theDigit).charAt(0);
			}

			int theSum = 0;
			for (int x=0;x<stringAsChars.length;x++)
			{
				theSum += Character.getNumericValue(stringAsChars[x]);
			}

			if (theSum%10==0)
				return true;
		}

		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string)
	{
		int firstNumber=0;
		int secondNumber=0;
		boolean gotFirst = false;
		int numberLength;

		for (int x=0;x<string.length();x++)
		{
			System.out.println("Charachter at x is " + string.charAt(x));

			if (gotFirst==false)
			{
				//put numeric value into firstNumber
				if (string.charAt(x)=='-')
				{
					String numAsStr = "";
					while (Character.isDigit(string.charAt(x+1)))
					{
						numAsStr = numAsStr.concat(string.substring(x+1,x+2));
						x++;
					}
					firstNumber = 0 - (Integer.parseInt(numAsStr));
					gotFirst = true;
					System.out.println("First number is " + firstNumber);
				}
				else if (Character.isDigit(string.charAt(x)))
				{
					String numAsStr = "";
					while (Character.isDigit(string.charAt(x)))
					{
						numAsStr = numAsStr.concat(string.substring(x,x+1));
						x++;
					}
					firstNumber = Integer.parseInt(numAsStr);
					gotFirst = true;
					System.out.println("First number is " + firstNumber);
				}
			}
			else
			{
				//put numeric value into secondNumber
				//put numeric value into firstNumber
				if (string.charAt(x)=='-')
				{
					String numAsStr = "";
					while (Character.isDigit(string.charAt(x+1)))
					{
						numAsStr = numAsStr.concat(string.substring(x+1,x+2));
						x++;
					}
					secondNumber = 0 - (Integer.parseInt(numAsStr));
					System.out.println("Second number is " + secondNumber);
				}
				else if (Character.isDigit(string.charAt(x)))
				{
					String numAsStr = "";
					while (Character.isDigit(string.charAt(x)))
					{
						numAsStr = numAsStr.concat(string.substring(x,x+1));
						x++;
					}
					secondNumber = Integer.parseInt(numAsStr);
					gotFirst = true;
					System.out.println("Second number is " + secondNumber);
				}
			}
		}

		//Lazy logic chain to handle what operation to do:
		if (string.contains("plus"))
		{
			return firstNumber + secondNumber;
		}
		else if (string.contains("minus"))
		{
			return firstNumber - secondNumber;
		}
		else if (string.contains("divided"))
		{
			return firstNumber/secondNumber;
		}
		else if (string.contains("mult"))
		{
			return firstNumber*secondNumber;
		}

		return 0;
	}

}
