package com.revature.eval.java.core;

import java.util.ArrayList;

public class TestingFile
{
        public static void main(String args[])
        {
            int myInt = 27;

            System.out.println("Value of int is " + myInt);

            testChange(myInt);

            System.out.println("Value of int is now " + myInt);
        }

        public static void testChange(int theValue)
        {
            theValue = 55;
        }
}
